# Regolith Linux Config Files

These are the staged Regolith Linux files. Note that the `.Xresources-regolith` file is also present in this repo. A symbolic link to this file has to be created from your `$HOME` directory.

These files are directly versioned in the `$HOME/.config/regolith` directory.

These files should only be installed on an unstaged Regolith install using the [companion restore script](https://gitlab.com/gdeflaux/regolith-config-restore/).
